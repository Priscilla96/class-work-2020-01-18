import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ISBN1 {

	 public static void main(String[] args) throws IOException {
	        long is;
	        int n = 0, i, t, d, dnum;
	        String dig;
	      
	        InputStreamReader in = new InputStreamReader(System.in);
	        BufferedReader br = new BufferedReader(in);
	        System.out.print("Input your ISBN number here(10 digits): ");
			
	        is = Long.parseLong(br.readLine());

	        
	        dig = "" + is;
	        if (dig.length() != 10) {
	            System.out.println("###...Invalid ISBN...###");
	            return;
	        }
	       
	        n = 0;
	        for (i = 0; i < dig.length(); i++) {
	            d = Integer.parseInt(dig.substring(i, i + 1));
	            dnum = i + 1;
	            t = dnum * d;
	            n += t;
	        }

	     
	        if ((n % 11) != 0) {
	            System.out.println(".....ISBN number is Correct.....");
	        } else {
	            System.out.println("## Invalid ISBN ##");
	        }
	    }
	}
